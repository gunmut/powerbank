/* 
 * File:   fsm.h
 * Author: sangkuriang
 *
 * Created on October 26, 2015, 2:01 PM
 */

#ifndef FSM_H
#define	FSM_H

#define OFF_STABLE 0
#define OFF_AWAL 1
#define ON_AWAL 2
#define PBON_LEDOFF1 3
#define PBON_LEDOFF2 4
#define PBON_LEDOFF3 5
#define PBON_LEDOFF4 6
#define PBON_LEDON1 7
#define PBON_LEDON2 8
#define PBON_LEDON3 9
#define PBON_LEDON4 10
#define TIMEOUT_ARUS 6000
#define TIMEOUT_PUSH 300

void fsm(int push, int arus, int batre, int *outputpb, int *outputsenter, int *state);

#endif	/* FSM_H */

