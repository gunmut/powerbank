#include "fsm.h"

/**
 * implementasi finite state machine dengan software
 * @param push push dari switch, active high
 * @param output output LED, active high
 * @param state state internal FSM. belum didefinisikan
 */
void fsm(int push, int arus, int batre, int *outputpb, int *outputsenter, int *state) {
    static int counter = 0;
    if(batre==0){
		*state = OFF_STABLE;
	}else{
		switch (*state) {
        case OFF_AWAL:
        {
            if (push == 0) {
                *state = OFF_STABLE;
            }
            break;
        }
        case OFF_STABLE:
        {
            if (push == 1) {
                *state = ON_AWAL;
            }
            break;
        }
        case ON_AWAL:
        {
            if (push == 0) {
                counter=0;
				*state = PBON_LEDOFF1;
            }
            break;
        }
        case PBON_LEDOFF1:
        {
            if ((push == 0)&(arus==0)) {
				*state = PBON_LEDOFF1;
                counter++;
                if (counter >= TIMEOUT_ARUS) {
                    *state = OFF_AWAL;
                }
            }
			if (push == 1) {
                counter=0;
				*state = PBON_LEDOFF2;
            }
            break;
        }
        case PBON_LEDOFF2:
        {
            if (push == 0) {
                counter=0;
				*state = PBON_LEDOFF3;
            }
            if (push == 1) {
				*state = PBON_LEDOFF2;
                counter++;
                if (counter >= TIMEOUT_PUSH) {
                    *state = OFF_AWAL;
                }
            }
            break;
        }
		case PBON_LEDOFF3:
        {
            if (push == 0) {
                *state = PBON_LEDOFF3;
                counter++;
                if (counter >= TIMEOUT_PUSH) {
                    *state = PBON_LEDOFF1;
                }
            }
            if (push == 1) {
				counter=0;
				*state = PBON_LEDOFF4;
            }
            break;
        }
		case PBON_LEDOFF4:
        {
            if (push == 0) {
                counter=0;
				*state = PBON_LEDON1;
            }
            if (push == 1) {
				*state = PBON_LEDOFF4;
                counter++;
                if (counter >= TIMEOUT_PUSH) {
                    *state = OFF_AWAL;
                }
            }
            break;
        }
		case PBON_LEDON1:
        {
            if (push == 0) {
				*state = PBON_LEDON1;
            }
			if (push == 1) {
                counter=0;
				*state = PBON_LEDON2;
            }
            break;
        }
        case PBON_LEDON2:
        {
            if (push == 0) {
                counter=0;
				*state = PBON_LEDON3;
            }
            if (push == 1) {
				*state = PBON_LEDON2;
                counter++;
                if (counter >= TIMEOUT_PUSH) {
                    *state = OFF_AWAL;
                }
            }
            break;
        }
		case PBON_LEDON3:
        {
            if (push == 0) {
                *state = PBON_LEDON3;
                counter++;
                if (counter >= TIMEOUT_PUSH) {
                    *state = PBON_LEDON1;
                }
            }
            if (push == 1) {
				counter=0;
				*state = PBON_LEDON4;
            }
            break;
        }
		case PBON_LEDON4:
        {
            if (push == 0) {
                counter=0;
				*state = PBON_LEDOFF1;
            }
            if (push == 1) {
				*state = PBON_LEDON4;
                counter++;
                if (counter >= TIMEOUT_PUSH) {
                    *state = OFF_AWAL;
                }
            }
            break;
        }
        default:
        {

        }
    }
    }
	// perhitungan output
    switch (*state) {
        case OFF_AWAL:
        case OFF_STABLE:
        {
            *outputpb = 0;
			*outputsenter = 0;
            break;
        }
        case ON_AWAL:
        case PBON_LEDOFF1:
		case PBON_LEDOFF2:
		case PBON_LEDOFF3:
		case PBON_LEDOFF4:
        {
            *outputpb = 1;
			*outputsenter = 0;          
            break;
        }
		case PBON_LEDON1:
		case PBON_LEDON2:
		case PBON_LEDON3:
		case PBON_LEDON4:
        {
            *outputpb = 1;
			*outputsenter = 1;          
            break;
        }
        default:
        {

        }
    }
}

