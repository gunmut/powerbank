/*****************************************************
Chip type               : ATmega328
Program type            : Application
AVR Core Clock frequency: 16.000000 MHz
Memory model            : Small
External RAM size       : N/A
Data Stack size         : N/A
 *****************************************************/
extern "C"{
#include "fsm.h"
}

const int sw = 2; //PD2
const int ledSenter = 3; //PD3
const int ledPb = 4; //PD4
const int sensorArus = 5; //PD5
const int sensorBatre = 6; //PD6
int state = OFF_STABLE;
int push;
int outputpb;
int outputsenter;
int arus;
int batre;


void setup() {
  // put your setup code here, to run once:
  pinMode(sw, INPUT);
  pinMode(ledSenter, OUTPUT);
  pinMode(ledPb, OUTPUT);
  pinMode(sensorArus, INPUT);
  pinMode(sensorBatre, INPUT);
  Serial.begin(9600);

  cli();//stop interrupts
  
  //set timer1 interrupt at 100Hz
  TCCR1A = 0;// set entire TCCR1A register to 0
  TCCR1B = 0;// same for TCCR1B
  TCNT1  = 0;//initialize counter value to 0
  // set compare match register for 2hz increments
  OCR1A = 19999;// = {(16*10^6)/(100*8)-1} (must be <65536)
  // turn on CTC mode
  TCCR1B |= (1 << WGM12);
  // Set CS11 bits for 8 prescaler
  TCCR1B |= (1 << CS11);
  // enable timer compare interrupt
  TIMSK1 |= (1 << OCIE1A);
  
  sei();//allow interrupts
}

void loop() {
  // put your main code here, to run repeatedly:
  //Serial.print("Push=");Serial.print(push);Serial.print(" ");Serial.print("PB=");Serial.print(outputpb);Serial.print(" ");Serial.print("Senter=");Serial.print(outputsenter);Serial.print(" ");Serial.print("State=");Serial.print(state);Serial.println(" ");
  //Serial.print("push");Serial.print("\t");Serial.print("pb");Serial.print("\t");Serial.print("senter");Serial.print("\t");Serial.print("state");Serial.println("\t");
  Serial.print(push);Serial.print("\t");Serial.print(outputpb);Serial.print("\t");Serial.print(outputsenter);Serial.print("\t");Serial.print(state);Serial.println("\t");
  //Serial.print(arus);Serial.print("\t");Serial.print(push);Serial.print("\t");Serial.print(outputpb);Serial.print("\t");Serial.print(outputsenter);Serial.print("\t");Serial.print(state);Serial.println("\t");
  delay(100);
}

ISR(TIMER1_COMPA_vect)
{
  if (digitalRead(sw)==HIGH) {
        push = 0;
  } else {
        push = 1;
  }
  arus = digitalRead(sensorArus);
  batre = digitalRead(sensorBatre);
  fsm(push, arus, batre, &outputpb, &outputsenter, &state);
  digitalWrite(ledPb, outputpb);
  digitalWrite(ledSenter, outputsenter);
  //Serial.print("PB=");Serial.print(outputpb);Serial.print(" ");Serial.print("Senter=");Serial.print(outputsenter);Serial.print(" ");Serial.print("State=");Serial.print(state);Serial.println(" ");

}

